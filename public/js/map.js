(function() {
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    ctx.canvas.width  = 500;
    ctx.canvas.height = 700;

    function drawLine([fromX, fromY, toX, toY]) {
        // ctx.beginPath();
        ctx.moveTo(fromY, fromX);
        ctx.lineTo(toY, toX);
        ctx.stroke();
        // ctx.closePath();
    }

    var lines = [
        //Cheo \
        [550, 150, 650, 250],
        [250, 50, 550, 350],
        [50, 50, 450, 450],
        [50, 250, 250, 450],

        // //Cheo /
        [250, 50, 50, 250],
        [450, 50, 50, 450],
        [550, 150, 250, 450],
        [650, 250, 550, 350],

        //Ngang
        [50, 50, 50, 450],
        [150, 50, 150, 450],
        [250, 50, 250, 450],
        [350, 50, 350, 450],
        [450, 50, 450, 450],
        [550, 150, 550, 350],

        //Doc
        [50, 50, 450, 50],
        [50, 150, 450, 150],
        [50, 250, 650, 250],
        [50, 350, 450, 350],
        [50, 450, 450, 450],

    ];

    lines.forEach(line => {
        drawLine(line);
    })
})()