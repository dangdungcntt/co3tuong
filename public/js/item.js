class Item {
    constructor(row, col, type) {
        this.row = row;
        this.col = col;
        this.type = type;
    }
}

export default Item;