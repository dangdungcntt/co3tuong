import {MAX_DEPTH, INF, _INF, MAX, MIN, TUONG, TOT, TRONG, RAO} from './constants.js';
import Policy from './policy.js';

class Node {
    constructor(matrix, isMax) {
        this.matrix = matrix;
        this.alpha = _INF;
        this.beta = INF;
        this.isMax = isMax;
    }

    getItem(row, col) {
        if (row < 0 || row > 7 || col < 0 || col > 6) {
            return null;
        }

        return this.matrix[row][col];
    }

    setType(matrix, item, type) {
        let { row, col} = item;
        matrix[row][col].type = type;
    }
    
    findChilds() {
        this.childs = [];
        let type = this.isMax ? TUONG : TOT;

        doubleLoop(1, 7, 1, 5, (i, j) => {
            let item = this.getItem(i, j);
            if (item.type != type) return;
            
            this.findMovable(item, it => {
                let newMatrix = clone(this.matrix);
                this.setType(newMatrix, item, TRONG);
                this.setType(newMatrix, it, type);
                
                this.childs.push({data: newMatrix, from: item, to: it});
            });
        });

    }

	calculate(depth = 0) {
		if (depth > MAX_DEPTH) {
            this.value = this.eval();
			return;
        }

        this.findChilds();

        let numChilds = this.childs.length;

        if (numChilds == 0) {
            this.value = this.eval();
            return;
        }
        
		if (this.isMax) {
			this.value = _INF;
		} else {
			this.value = INF;
		}

		this.new_alpha = this.alpha;
        this.new_beta = this.beta;
	
		for (let i = 0; i < numChilds; i++) {
            // this.from = from;
            // this.to = to;
			if (this.new_alpha >= this.new_beta) {
				// child.cutted = true;
				continue;
            }
            
            let {data: matrix, from, to} = this.childs[i];
            let child = new Node(matrix, !this.isMax);

			child.alpha = this.new_alpha;
			child.beta = this.new_beta;
	
            child.calculate(depth + 1);

            if (to.type == TOT) {
                child.value += 60;
            }
            
            
			if (this.isMax) {
                if (child.value > this.value) {
                    this.from = from;
                    this.to = to;
                }
				this.new_alpha = Math.max(this.new_alpha, child.value);
                this.value = Math.max(this.value, child.value);
                
			} else {
                if (child.value < this.value) {
                    this.from = from;
                    this.to = to;
                }
				this.new_beta = Math.min(this.new_beta, child.value);
                this.value = Math.min(this.value, child.value);
			}
		}
    }
    
    findMovable(item, callback) {

        let { row, col } = item;

        if (this.isMax == 1) {
            doubleLoop(row - 2, row + 2, col - 2, col + 2, (i, j) => {
                tap(this.getItem(i, j), it => {

                    let between = this.getItemBetween(item, it);

                    if (Policy.canMove(item, it, between) === true) {
                        callback(it);
                        return;
                    }
                });
            });

            return;
        }

        doubleLoop(row - 1, row + 1, col - 1, col + 1, (i, j) => {
            tap(this.getItem(i, j), it => {
                if (Policy.canMove(item, it) === true) {
                    callback(it);
                }
            })
        });
    }

    getItemBetween(a, b) {
        let row = (a.row + b.row) / 2;
        let col = (a.col + b.col) / 2;

        if (row != parseInt(row) || col != parseInt(col)) {
            return null;
        }

        return this.getItem(row, col);
    }

    eval() {
        let totalPoint = 0;
        let str = '';

        doubleLoop(1, 7, 1, 5, (i, j) => {
            let item = this.getItem(i, j);
            if (item.type == TUONG) {
                let point = this.calculateScoretAt(i, j);
                str += point + ' ';
                totalPoint += point;
            }
        });
        // console.log(str + ' ' + totalPoint);
        return totalPoint;
    }

    calculateScoretAt(row, col) {
        let point = 0;

        if ((row + col) % 2 != 0) {
            point -= 5;
        }

        if ((row == 5 || row == 1) && (col == 1 || col == 5)) {
            point -= 30
        }

        if ((row == 1 && col == 1) || (row == 1 && col == 5) || (row == 5 && col == 1) || (row == 5 && col == 5)) {
            point -= 80;
        }

        let count = 0;

        doubleLoop(0, 7, 0, 6, (i, j) => {
            if (i == row && j == col) return;

            tap(this.getItem(i, j), it => {
                count += it.type == TOT;
            })
        });

        point -= count * 3;

        if (row > 5) {
            point -= 80;
        }

        if (random(1) < 0.05) {
            point += 50;
        }

        doubleLoop(row - 1, row + 1, col - 1, col + 1, (i, j) => {
            if (i == row && j == col) return;

            tap(this.getItem(i, j), it => {
                if (it.type == TOT) {
                    point -= 3;
                } else if (it.type == RAO) {
                    point -= 3;
                }
            })
        })

        return point;
    }

}

export default Node;