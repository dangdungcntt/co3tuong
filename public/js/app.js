import {TOT, TUONG, RAO, TRONG, COTHEDI, INF, _INF, MAX, MIN} from './constants.js'
import Board from './board.js'
import Policy from './policy.js';
import AlphaBeta from './AlphaBeta.js';

var app = new Vue({
    el: '#app',
    data() {
        return {
            grids: Board.init(),
            TUONG: TUONG,
            TOT: TOT,
            RAO: RAO,
            TRONG: TRONG,
            selected: false,
            selectedItem: this.defaultSelected(),
            moving: false,
            level: +((new URLSearchParams(location.search)).get('level') || 2) 
        }
    },
    created() {
        this.moving = true;
        setTimeout(() => {

            switch (this.level) {
                case 1:
                    this.tuong();
                    break;
                default :
                    this.tuongV2();
                    break;
            }


            // this.tuong();
        }, 1000);
    },
    methods: {

        getItem(row, col) {
            if (row < 0 || row > 7 || col < 0 || col > 6) {
                return null;
            }

            return this.grids[row][col];
        },

        getItemBackgroud(type) {
            switch (type) {
                case TOT:
                    return 'images/android.png';
                case TUONG:
                    return 'images/ios.png';
                default :
                    return 'images/transparent.png';
            }
            
        },

        clickItem(row, col) {
            //đang di chuyển một quân khác
            if (this.moving) return;

            let targetItem = this.getItem(row, col);

            //quân tướng không có action chọn
            if (targetItem.type == TUONG) return;

            if (this.selected) { //đã chọn 1 quân
                this.clickWhenSelectedItem(targetItem);
                return;
            }

            if (targetItem.type == TOT) {
                this.findMoveable(targetItem);
                this.toggleSelect(true, targetItem);
            }
        },

        clickWhenSelectedItem(targetItem) {
            //đã chọn 1 quân

            let currentSelectedItemRow = this.selectedItem.row;
            let currentSelectedItemCol = this.selectedItem.col;
            console.log(currentSelectedItemCol, currentSelectedItemRow);

            if (currentSelectedItemRow == targetItem.row && currentSelectedItemCol == targetItem.col) { //chọn vào ô đã chọn
                //hủy chọn
                console.log('Hủy chọn')

                this.removeAllMoveable(); //xóa tất cả đánh dấu có thể di chuyển
                this.toggleSelect(false); //xóa selectedItem
                return;
            }

            //bấm vào ô không phải ô đã chọn

            if (!targetItem.moveable) { //Nếu không thể đi được
                console.log('Vị trí di chuyển không hợp lệ', targetItem.row, targetItem.col);
                return;
            }

            //có thể đi được
            
            let cachedSelectedItem = this.selectedItem;
            this.toggleSelect(false);
            this.removeAllMoveable();

            this.moving = true;
            //move tạm thời để tạo animation (dựa vào css transition)
            this.moveItem(cachedSelectedItem, targetItem, () => {
                this.moving = false;

                switch (this.level) {
                    case 1:
                        this.tuong();
                        break;
                    default :
                        this.tuongV2();
                        break;
                }
            });

            //move thực tế
            //trả lại về vị trí cũ và hoán đổi TYPE của 2 ô
            

            return;
        },

        moveItem(from, to, callback) {
            let oldRow = from.row;
            let oldCol = from.col;
            let oldType = from.type;

            from.row = to.row;
            from.col = to.col;

            setTimeout(() => {
                from.row = oldRow;
                from.col = oldCol;
                from.type = TRONG;

                to.type = oldType;

                if (callback) {
                    callback();
                }
                
            }, 300);
        },

        toggleSelect(flag, item) {
            if (flag) {
                this.selected = true;
                this.selectedItem = item;

            } else {
                this.selected = false;
                this.selectedItem = this.defaultSelected();
            }
        },

        findMoveable(item) {

            let { row, col } = item;

            doubleLoop(row - 1, row + 1, col - 1, col + 1, (i, j) => {
                tap(this.getItem(i, j), it => {
                    if (Policy.canMove(item, it) === true) {
                        it.moveable = COTHEDI;
                    }
                })
            });
        },

        removeAllMoveable() {
            doubleLoop(0, 7, 0, 6, (i, j) => {
                tap(this.getItem(i, j), it => {
                    it.moveable = false;
                })
            });
        },

        defaultSelected() {
            return { row: -1, col: -1, type: -1 };
        },

        tuongV2() {
            let result = AlphaBeta.f(this.grids, MAX);

            if (result == false) {
                alert("YOU WIN");
                return;
            }
            let {from, to} = result;
            if (!from || !to) {
                alert("YOU WIN");
                return;
            }
            this.moving = true;
            this.moveItem(from, to);

            setTimeout(() => {
                this.moving = false;
            }, 320);
        },

        //Quân tướng
        tuong() {
            console.log("TUONG--------------NEW TURN");
            this.moving = true;
            let dsTuong = [];

            doubleLoop(0, 7, 0, 6, (i, j) => {
                tap(this.getItem(i, j), it => {
                    if (it.type == TUONG) {
                        dsTuong.push(it);
                    }
                })
            })

            let results = [];
            let firstResult = this.findItemToGo(dsTuong[0]);

            let maxScore = firstResult.score;

            results.push(firstResult);

            loop(1, dsTuong.length - 1, i => {
                let result = this.findItemToGo(dsTuong[i]);

                results.push(result);

                if (result.score > maxScore) {
                    maxScore = result.score;
                }
            })

            console.log("TUONG--RESULT ", results)

            if (maxScore == -1e9) {
                alert("YOU WIN");
                return;
            }

            let arrayMax = [];

            loop(0, results.length - 1, i => {
                if (results[i].score == maxScore) {
                    arrayMax.push(i);
                }
            })

            console.log("TUONG--ARRAY MAX", arrayMax);

            let randomIndex = random(arrayMax.length - 1);
            console.log("TUONG--RANDOM INDEX ", randomIndex);

            this.removeAllMoveable();
            this.moveItem(dsTuong[arrayMax[randomIndex]], results[arrayMax[randomIndex]].targetItem);

            setTimeout(() => {
                this.moving = false;
            }, 320);

        },

        findItemToGo(item) {
            console.log("TUONG--findItemToGo ", item)

            //V1: tham lam (depth = 1)
            let maxScore = -1e9;
            let targetItem = null;

            let {row, col} = item;

            doubleLoop(row - 2, row + 2, col - 2, col + 2, (i, j) => {
                tap(this.getItem(i, j), it => {
                    console.log("TUONG--set item ", it)

                    let between = this.getItemBetween(item, it);

                    console.log("TUONG--between item and it ", between);

                    if (Policy.canMove(item, it, between) !== true) {
                        return;
                    }

                    let score = this.calculateScoretAt(it.row, it.col);

                    console.log("TUONG--item it score ", score)

                    if (it.type == TOT) {
                        console.log("------------------------------ +70");  
                        score += 70;
                    }

                    if (score > maxScore) {
                        maxScore = score;
                        targetItem = it;
                    }

                    it.moveable = COTHEDI;
                });
            });

            return {
                score: maxScore,
                targetItem
            }
        },

        calculateScoretAt(row, col) {
            let point = 0;

            if ((row + col) % 2 != 0) {
                point -= 5;
            }

            if ((row == 5 || row == 1) && (col == 1 || col == 5)) {
                point -= 20
            }

            let count = 0;

            doubleLoop(0, 7, 0, 6, (i, j) => {
                if (i == row && j == col) return;

                tap(this.getItem(i, j), it => {
                    count += it.type == TOT;
                })
            });

            point -= count * 3;

            doubleLoop(row - 1, row + 1, col - 1, col + 1, (i, j) => {
                if (i == row && j == col) return;

                tap(this.getItem(i, j), it => {
                    if (it.type == TOT) {
                        point -= 3;
                    } else if (it.type == RAO) {
                        point -= 1;
                    }
                })
            })

            return point;
        },

        getItemBetween(a, b) {
            let row = (a.row + b.row) / 2;
            let col = (a.col + b.col) / 2;

            if (row != parseInt(row) || col != parseInt(col)) {
                return null;
            }

            return this.getItem(row, col);
        }
    }
  
  });