import { _INF } from './constants.js';
import Policy from './policy.js';
import Node from './Node.js';

export default {

    f(matrix, isMax) {
        let root = new Node(matrix, isMax);
        root.calculate(isMax);
        console.log(root);

        if (root.childs.length == 0) {
            return false;
        }

        return {
            from: root.from, to: root.to
        };

    },

    value(matrix) {
        return 1;
    },

    
}