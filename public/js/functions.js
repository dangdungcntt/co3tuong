function doubleLoop(startI, endI, startJ, endJ, callback) {
    for (let i = startI; i <= endI; i++) {
        for(let j = startJ; j <= endJ; j++) {
            if (callback(i, j) === false) {
                break;
            };
        }
    }
}

function loop(start, end, callback) {
    for (let i = start; i <= end; i++) {
        if (callback(i) === false) {
            break;
        }
    }
}

function tap(value, callback) {
    if (value) {
        callback(value);
    }

    return value;
}

function random(max, min = 0) {
    return Math.round(Math.random() * max + min);
}

function clone(a) {
    return JSON.parse(JSON.stringify(a));
}