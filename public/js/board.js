import Item from './item.js';
import { TOT, TUONG, RAO, TRONG } from './constants.js';

export default {
    init() {
        return [
            [new Item(0, 0, RAO), new Item(0, 1, RAO), new Item(0, 2, RAO), new Item(0, 3, RAO), new Item(0, 4, RAO), new Item(0, 5, RAO), new Item(0, 6,RAO)],
            [new Item(1, 0, RAO), new Item(1, 1, TOT), new Item(1, 2, TOT), new Item(1, 3, TOT), new Item(1, 4, TOT), new Item(1, 5, TOT), new Item(1, 6, RAO)],
            [new Item(2, 0, RAO), new Item(2, 1, TOT), new Item(2, 2, TOT), new Item(2, 3, TRONG), new Item(2, 4, TOT), new Item(2, 5, TOT), new Item(2, 6, RAO)],
            [new Item(3, 0, RAO), new Item(3, 1, TOT), new Item(3, 2, TRONG), new Item(3, 3, TRONG), new Item(3, 4, TRONG), new Item(3, 5, TOT), new Item(3, 6, RAO)],
            [new Item(4, 0, RAO), new Item(4, 1, TOT), new Item(4, 2, TOT), new Item(4, 3, TRONG), new Item(4, 4, TOT), new Item(4, 5, TOT), new Item(4, 6, RAO)],
            [new Item(5, 0, RAO), new Item(5, 1, TOT), new Item(5, 2, TOT), new Item(5, 3, TRONG), new Item(5, 4, TOT), new Item(5, 5, TOT), new Item(5, 6, RAO)],         
            [new Item(6, 0, RAO), new Item(6, 1, RAO), new Item(6, 2, TUONG), new Item(6, 3, TRONG), new Item(6, 4, TUONG), new Item(6, 5, RAO), new Item(6, 6, RAO)],
            [new Item(7, 0, RAO), new Item(7, 1, RAO), new Item(7, 2, RAO), new Item(7, 3, TUONG), new Item(7, 4, RAO), new Item(7, 5, RAO), new Item(7, 6, RAO)]
        ]
    }
}