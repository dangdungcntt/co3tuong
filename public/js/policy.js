import { TOT, TUONG, RAO, TRONG } from './constants.js';

let cannotMoveLine = [
    '6.2-5.1', '5.1-6.2',
    '6.2-5.2', '5.2-6.2',

    '6.4-5.4', '5.4-6.4',
    '6.4-5.5', '5.5-6.4',
]
    

export default {
    canMove(from, to, between) {

        //Không thể đi đến ô rào
        if (to.type == RAO) {
            return false;
        }

        if (from.type == TUONG) {
            return this.tuongCanMove(from, to, between);
        }

        if (from.type == TOT) {
            return this.totCanMove(from, to);
        }

        return false;
    },

    totCanMove(from, to) {
        //tốt không thể ăn tốt
        if (to.type == TOT) {
            return "Tốt không ăn được tốt";
        }

        //Không được ăn tướng
        if (to.type == TUONG) {
            return "Tốt không ăn được tướng";
        }

        if (this.notExistsLine(from, to)) {
            return false;
        }

        let distanceRow = Math.abs(from.row - to.row);
        let distanceCol = Math.abs(from.col - to.col);
        let sum = distanceRow + distanceCol;

        if (sum == 0 || distanceCol > 1 || distanceRow > 1) {
            return false;
        }

        if (sum == 2) { //đang đi chéo
            return (from.row + from.col) % 2 == 0;
        }

        return true;
    },

    tuongCanMove(from, to, between) {
        //tướng không thể ăn tướng
        if (to.type == TUONG) {
            return "Tướng không ăn được tướng";
        }

        if (this.notExistsLine(from, to)) {
            return false;
        }

        let distanceRow = Math.abs(from.row - to.row);
        let distanceCol = Math.abs(from.col - to.col);
        let sum = distanceRow + distanceCol;

        //tối đa đi 2 ô
        if (distanceCol > 2 || distanceRow > 2) {
            return false;
        }

        //chỉ đi chéo 2 ô
        if (sum == 3) { //như này là đang đi giống quân mã trong cờ tướng => không cho phép
            return false;
        }

        //Nếu ăn tốt thì phải cách 2 ô
        if (to.type == TOT && (distanceCol == 1 || distanceRow == 1)) {
            return false;
        }

        //Nếu đi đến ô trống thì chỉ đi 1 bước
        if (to.type == TRONG && (distanceCol == 2 || distanceRow == 2)) {
            return false;
        }

        
        if (between != null) {
            if (to.type == TOT && between.type != TRONG) {
                return false;
            }

            if (this.notExistsLine(from, between) || this.notExistsLine(between, to)) {
                return false;
            }
        }

        if (sum == 2 || sum == 4) { //đang đi chéo
            //kiểm tra tồn tại đường đi không
            return (from.row + from.col) % 2 == 0;
        }

        return true;
    },

    /*
        @return true nếu như không có đường đi
     */
    notExistsLine(from, to) {
        // console.log(from, to);

        return cannotMoveLine.includes(`${from.row}.${from.col}-${to.row}.${to.col}`) || 
               cannotMoveLine.includes(`${to.row}.${to.col}-${from.row}.${from.col}`);
    }
}

